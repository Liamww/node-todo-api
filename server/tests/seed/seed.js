const jwt = require('jsonwebtoken');
const { ObjectID } = require('mongodb');

const { Todo } = require('../../models/todo');
const { User } = require('../../models/user');

const userOneId = new ObjectID();
const userTwoId = new ObjectID();
const users = [
    { 
        _id: userOneId, 
        email: 'liam.wohlfart0@gmail.com', 
        password: 'userOnePass',
        tokens: [{ 
            access: 'auth',
            token: jwt.sign({ _id: userOneId, access: 'auth', }, process.env.JWT_SECRET).toString()
        }],
    },
    {
        _id: userTwoId,
        email: 'lorem@ipsum.com',
        password: 'userTwoPass',
        tokens: [{ 
            access: 'auth',
            token: jwt.sign({ _id: userTwoId, access: 'auth', }, process.env.JWT_SECRET).toString()
        }],
    }
];

const todos = [
    { text: 'First test todo', _id: new ObjectID(), completed: true, completedAt: 333, _creator: userOneId },
    { text: 'Second test todo', _id: new ObjectID(), _creator: userTwoId }
];

const populateUsers = done => {
    User.remove({}).then(() => {
        const userOne = new User(users[0]).save();
        const userTwo = new User(users[1]).save();

        return Promise.all([userOne, userTwo]);
    }).then(() => done());
};


const populateTodos = done => {
    Todo.remove({}).then(() => {
        return Todo.insertMany(todos);
    }).then(() => done());
};


module.exports = {
    todos,
    populateTodos,
    users,
    populateUsers
};
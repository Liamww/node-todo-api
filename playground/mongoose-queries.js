const { mongoose } = require('../server/db/mongoose');
const { Todo } = require('../server/models/todo');
const { User } = require('../server/models/user');

const id = '597703d7a1135822804b3374aaa';

// Todo.find({ 
//     _id: id
// }).then(todos => {
//     console.log('Todos', todos);
// });

// Todo.findOne({ 
//     _id: id
// }).then(todo => {
//     console.log('Todo', todo);
// });

// Todo.findById(id).then(todo => {
//     if (!todo) {
//         return console.log('Id not found');
//     }
//     console.log('Todo by ID', todo);
// }).catch(err => {
//     console.log(err);
// });

User.findById('59770b8e43053723c0b441f9').then(user => {
    if (!user) {
        return console.log('Unable to found user');
    }
    console.log('User by ID', user);
}).catch(err => {
    console.log(err);
});
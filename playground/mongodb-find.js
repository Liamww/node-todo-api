const MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if (err) {
        return console.log('Unable to connect to mongo');
    }
    console.log('Connected to mongodb');

    db.collection('Todos').find({completed: false}).toArray().then(documents => {
        console.log('Todos');
        console.log(JSON.stringify(documents, undefined, 4));
    }).catch(err => {
        console.log('Unable to fetch todos', err)
    });
});

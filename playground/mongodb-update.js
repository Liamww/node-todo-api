const MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if (err) {
        return console.log('Unable to connect to mongo');
    }
    console.log('Connected to mongodb');

    db.collection('Users').findOneAndUpdate({
        name: 'Riamu'
    }, {
        $set: {
            name: 'Liam'
        },
        $inc: {
            age: -1
        }
    }, {
        returnOriginal: false
    }).then(result => {
        console.log(result);
    });

    db.close();
});

const { SHA256 } = require('crypto-js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const password = '123abc!';

// bcrypt.genSalt(10, (err, salt) => {
//     bcrypt.hash(password, salt, (err, hash) => {
//         console.log(hash);
//     });
// });

const hash = '$2a$10$zOTlUWwFq2yXsp1obGXv3.iVLLsdfGkH16/w5vVJdymFpsqsnkLQS';

bcrypt.compare(password, hash, (err, result) => {
    console.log(result);
});

// let data = {
//     id: 10
// };

// const token = jwt.sign(data, '123abc');
// console.log(token);

// const decoded = jwt.verify(token, '123abc');
// console.log(decoded);

// const message = 'I am User number 3';
// const hash = SHA256(message).toString();

// console.log(`Message: ${message}`);
// console.log(`Hash: ${hash}`);

// let data = {
//     id: 4
// };

// let token = {
//     data,
//     hash: SHA256(JSON.stringify(data + 'geheim')).toString()
// }

// token.data.id = 5;

// let resultHash = SHA256(JSON.stringify(token.data + 'geheim')).toString();

// if (resultHash === token.hash) {
//     console.log('Zugriff gewährt');
// } else {
//     console.log('Tokens sind nicht gleich. Zugriff verweigert.')
// }